﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDTO
    {
        public int id_produto { get; set; }
        public string nm_produto { get; set; }
        public decimal vl_preco { get; set; }
    }
}
﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabse
    {
        public void Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto(nm_produto, vl_preco) VALUES(@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));
            parms.Add(new MySqlParameter("vl_preco", dto.vl_preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}